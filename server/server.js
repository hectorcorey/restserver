require('./config/config');

const express = require('express');
const app = express();
const mongoose = require('mongoose');



const bodyParser = require('body-parser');


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())
 
//configuracion de rutas
app.use( require('./routes/index'));
 

mongoose.connect(process.env.urlBD,
    { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true},
    (error, res)=>{
        if(error) throw error;

        console.log("Base de datos Online");
    });

app.listen(process.env.PORT,()=>{
    console.log('Escuchando en el 3000');
    
})