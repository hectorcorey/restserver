///=================================
/// Puerto
///=================================



process.env.PORT = process.env.PORT || 3000;



///=================================
/// Entorno
///=================================

process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

///=================================
/// base de datos
///=================================

if(process.env.NODE_ENV === 'dev'){
    urlBD = 'mongodb://localhost:27017/cafe';
}
else{
    urlBD = process.env.MONGO_URI;
}
//urlBD ='mongodb+srv://hectorcorey:lZjtT9bSPReskVrk@cluster0.bvmzq.mongodb.net/cafe';
process.env.urlBD = urlBD;



/// variables de entorno
/// heroku config:set MONGO_URI=""


///mongodb+srv://hectorcorey:lZjtT9bSPReskVrk@cluster0.bvmzq.mongodb.net/cafe
///hectorcorey
///lZjtT9bSPReskVrk


///=================================
/// Vencimiento del Token
///=================================

process.env.CADUCIDAD_TOKEN = 60 * 60 * 24 * 30;

///=================================
/// Seed de autenticación
///=================================
'este-es-el-seed-desarrollo'
process.env.SEED = process.env.SEED || 'este-es-el-seed-desarrollo';

/// heroku config:set SEED="este-es-el-seed-desarrollo"

